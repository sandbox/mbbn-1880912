Jalali_Fullcalendar
===================

Jalali Fullcalendar for Drupal

Requirement
===================
datex	http://drupal.org/sandbox/drupalion/1841798
fullcalendar	http://drupal.org/project/fullcalendar


Install Jalali fullcalendar plugin
===================
   drush  download-js
OR
   download jfullcalendar.js , jfullcalendar.css from https://github.com/mbbn/jalali_fullcalendar
   and get to sites/all/libraries/fullcalendar.

Install Jalali Fullcalendar